//[OBJECTIVE] Create a server-side app useing Express Web Framework.

//Relate this task to something that you do on a daily basis
//[SECTION]append the entire app to our node package manager.

//1. Identify and prepare the ingredients.
	const express = require("express");

//express => will be used as the main component to create the server.
//we need to be able to gather/acquire the utilities and components needed that the express library will provide us.
	//=> require() -> directive used to get the library/component needed inside the module

	//prepare the environment in which the project will be served

//[SECTION] preparing a remote repository for our Node project.
	//NOTE: always DISABLE the node_modules folder
	//WHY?
		//-> it will take up too much space in our repository making it a lot more difficult to stage upon commiting the changes in our remote repo.
		//-> If ever that you will deploy your node project on deployment platforms (heroku,netlify.vercel) the project will automatically be rejected because node_modules is not recognized on various deployment platforms.

//[SECTION] Create a Runtime environment that automatically autofix all the
//changes in our app.

	//package.json -> the "heart"  of every node project. this also contains different meta data that describes the structure of the project.

	//scripts -> is used to declare and describe custom commands and keywords that can be used to execute this project with the correct runtime environment.

	//NOTE: "start" is globally recognized amongst node projects and frameworks as the `default` command script to execute a task/project. 
	//however for *unconvetional* keywords or commands you have to append the command "run"
	//SYNTAX: npm run <custom command>

//we are going to use a utility called nodemon.
//upon starting the entry point module with nodemon you will be able to 'append'
//the application with the proper run time environment. allowing you to save time
//and effort upon commiting changes to your app.
/*console.log(`This will be our server.`);
console.log (`New Changes`);
console.log (`gg pilipinas`);
console.log (`hindi wonderful tonight`);*/

//you can even insert items like text art into your run time environment

console.log(`
	Welcome to our Express API Server
	───────▄▀▀▀▀▀▀▀▀▀▀▄▄
	────▄▀▀░░░░░░░░░░░░░▀▄
	──▄▀░░░░░░░░░░░░░░░░░░▀▄
	──█░░░░░░░░░░░░░░░░░░░░░▀▄
	─▐▌░░░░░░░░▄▄▄▄▄▄▄░░░░░░░▐▌
	─█░░░░░░░░░░░▄▄▄▄░░▀▀▀▀▀░░█
	▐▌░░░░░░░▀▀▀▀░░░░░▀▀▀▀▀░░░▐▌
	█░░░░░░░░░▄▄▀▀▀▀▀░░░░▀▀▀▀▄░█
	█░░░░░░░░░░░░░░░░▀░░░▐░░░░░▐▌
	▐▌░░░░░░░░░▐▀▀██▄░░░░░░▄▄▄░▐▌
	─█░░░░░░░░░░░▀▀▀░░░░░░▀▀██░░█
	─▐▌░░░░▄░░░░░░░░░░░░░▌░░░░░░█
	──▐▌░░▐░░░░░░░░░░░░░░▀▄░░░░░█
	───█░░░▌░░░░░░░░▐▀░░░░▄▀░░░▐▌
	───▐▌░░▀▄░░░░░░░░▀░▀░▀▀░░░▄▀
	───▐▌░░▐▀▄░░░░░░░░░░░░░░░░█
	───▐▌░░░▌░▀▄░░░░▀▀▀▀▀▀░░░█
	───█░░░▀░░░░▀▄░░░░░░░░░░▄▀
	──▐▌░░░░░░░░░░▀▄░░░░░░▄▀
	─▄▀░░░▄▀░░░░░░░░▀▀▀▀█▀
	▀░░░▄▀░░░░░░░░░░▀░░░▀▀▀▀▄▄▄▄▄
	GOODLUCK NALANG PILIPINAS
`)